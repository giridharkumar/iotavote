const mongoose = require('mongoose');
const Schema = mongoose.Schema;
const regionSchema = new Schema({
    region_name: {
        type: String,
        trim: true
    },
    status: {
        type: String,
        emum: ['Active', 'Inactive']
    },
    created_on: {
        type: Date,
        default: new Date()
    },
    modified_on: {
        type: Date
    }
});

mongoose.model('region',regionSchema);