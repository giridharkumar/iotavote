const mongoose = require('mongoose');
const Schema = mongoose.Schema;
const partyMemberSchema = new Schema({
    name: {
        type: String,
        trim: true
    },
    email: {
        type: String,
        trim: true
    },
    member_pic: {
        type: String,
        trim: true
    },
    status: {
        type: String,
        enum: ['Active', 'Inactive']
    },
    sub_role: {
        type: String,
        trim: true
    },
    is_goverment: {
        type: Number
    },
    created_on: {
        type: Date,
        default: new Date()
    },
    party_id: {
        type: String,
        trim: true
    },
    party_role_id: {
        type: String,
        trim: true
    },
    public_role_id: {
        type: String,
        trim: true
    },
    region_id: {
        type: String,
        trim: true
    },
    goverment_id: {
        type: String,
        trim: true
    },
    background_image: {
        type: String,
        trim: true
    }
});
mongoose.model('partymember',partyMemberSchema);