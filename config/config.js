let config = {
  development: {
    'port': 3001,
    'db': 'mongodb://localhost/iotavote'
  },
  'likedTextForParty':'You Love most - Nation Loves Most',
  'dislikedTextForParty':'You Dislike Most - Nation Dislikes Most',
  'likedTextForCandidate':'You liked most',
  'dislikedTextForCandidate':'You liked least'
};
module.exports = config;
