const mongoose = require('mongoose');
const logger = require('../utils/logger.js');
const restResponse = require('../utils/restresponse.js');
const party = mongoose.model('party');
const poll = mongoose.model('poll');
const Promise = require('promise');
const partymember = mongoose.model('partymember');
const Government = mongoose.model('Government');
const User = mongoose.model('user');
const Category = mongoose.model('category');
const config = require('../config/config.js')

function DashboardController() {
    this.getFavouriteCandidate = function(req, res) {
        let favCandidateResponseObj = {
            score: [],
            msg: {
                liked: config.likedTextForCandidate,
                disliked: config.dislikedTextForCandidate
            }

        };
        let scoreObj = {};
        let userId = req.params.userId;
        let positiveResForUserScore = 0,
            negativeResForUserScore = 0,
            neutralResForUserScore = 0,
            positiveResForNationScore = 0,
            negativeResForNationScore = 0,
            neutralResForNationScore = 0;

        let userScore = 0,
            nationScore = 0,
            promoterTotal = 0,
            absoluteTotal = 0;

        poll.aggregate([{
            $group: {
                _id: "$member_id",
                "polls": {
                    $push: "$$ROOT"
                }
            }
        }], function(err, data) {
            if (err) {
                console.log('error in poll aggregate', err);
                logger.error(err);
            } else {
                if (data && data !== null && data.length) {
                    let memberIds = [];
                    //iteration over party list
                    data.forEach(i => {
                        memberIds.push(i._id);
                        let pollsList = i.polls;
                        let scoreObj = {
                            id: i._id
                        };
                        // iteration over poll list
                        if (pollsList && pollsList.length) {


                            for (let j = 0; j < pollsList.length; j++) {
                                let poll = pollsList[j];
                                let userResponseList = poll.userResponse;
                                if (userResponseList && userResponseList.length) {
                                    //iteration over user response list
                                    for (let k = 0; k < userResponseList.length; k++) {
                                        let userResponseObj = userResponseList[k];
                                        if (userResponseObj.selectedOptionValue === 1) {
                                            if (userResponseObj.userId === userId) {
                                                positiveResForUserScore += (1 * userResponseObj.userDefinedWeightage);
                                                positiveResForNationScore += (1 * userResponseObj.userDefinedWeightage);
                                            } else {
                                                positiveResForNationScore += (1 * userResponseObj.userDefinedWeightage);
                                            }

                                        }
                                        if (userResponseObj.selectedOptionValue === -1) {
                                            if (userResponseObj.userId === userId) {
                                                negativeResForUserScore += (1 * userResponseObj.userDefinedWeightage);
                                                negativeResForNationScore += (1 * userResponseObj.userDefinedWeightage);
                                            } else {
                                                negativeResForNationScore += (1 * userResponseObj.userDefinedWeightage);
                                            }

                                        }
                                        if (userResponseObj.selectedOptionValue === 0) {
                                            if (userResponseObj.userId === userId) {
                                                neutralResForUserScore += (1 * userResponseObj.userDefinedWeightage);
                                                neutralResForNationScore += (1 * userResponseObj.userDefinedWeightage);
                                            } else {
                                                neutralResForNationScore += (1 * userResponseObj.userDefinedWeightage);
                                            }

                                        }
                                    } //closing user response list iteration
                                }
                            }
                        }
                        if (positiveResForUserScore || negativeResForUserScore || neutralResForUserScore) {
                            promoterTotal = positiveResForUserScore - negativeResForUserScore - neutralResForUserScore;
                            absoluteTotal = positiveResForUserScore + negativeResForUserScore + neutralResForUserScore;
                            userScore = promoterTotal / absoluteTotal;
                            promoterTotal = positiveResForNationScore - negativeResForNationScore - neutralResForNationScore;
                            absoluteTotal = positiveResForNationScore + negativeResForNationScore + neutralResForNationScore;
                            nationScore = promoterTotal / absoluteTotal;
                            scoreObj.user_score = userScore * 100;
                            scoreObj.nation_score = userScore * 100;
                        } else {
                            scoreObj.user_score = 0;
                            scoreObj.nation_score = 0;
                        }

                        favCandidateResponseObj.score.push(scoreObj);

                    });
                    partymember.find({
                        _id: {
                            $in: memberIds
                        }

                    }).then(candidates => {
                        if (candidates && candidates.length) {
                            candidates.forEach(candidate => {
                                favCandidateResponseObj.score.map(i => {
                                    if (candidate._id.toString() === i.id.toString()) {
                                        i.name = candidate.name;
                                        i.user_image = candidate.member_pic;
                                    }
                                });
                            });
                        }
                        restResponse.sendJsonSuccessResponse(res, favCandidateResponseObj);
                    }).catch(err => {
                        logger.error('Error in finding members: ', err);
                        restResponse.sendErrorResponse(res, 500, err);
                    });

                }
            }
        });

    }
    this.getFavouriteParty = function(req, res) {

        let userId = req.params.userId;
        let positiveResForUserScore = 0,
            negativeResForUserScore = 0,
            neutralResForUserScore = 0,
            positiveResForNationScore = 0,
            negativeResForNationScore = 0,
            neutralResForNationScore = 0;

        let userScore = 0,
            nationScore = 0,
            promoterTotal = 0,
            absoluteTotal = 0;
        // response object
        let favPartyResponseObj = {
            score: [],
            msg: {
                liked: config.likedTextForParty,
                disliked: config.dislikedTextForParty
            }

        };

        poll.aggregate([{
            $group: {
                _id: "$party_id",
                "polls": {
                    $push: "$$ROOT"
                }
            }
        }], function(err, data) {
            if (err) {
                restResponse.sendErrorResponse(res, 500, err);
            } else {
                if (data && data !== null && data.length) {
                    let partyIds = [];
                    //iteration over data(polls group by party_id)
                    data.forEach(i => {
                        partyIds.push(i._id);
                        let pollsList = i.polls;
                        let scoreObj = {
                            id: i._id
                        };

                        if (pollsList && pollsList.length) {
                            // iteration over poll list of party
                            for (let j = 0; j < pollsList.length; j++) {
                                let poll = pollsList[j];
                                let userResponseList = poll.userResponse;
                                if (userResponseList && userResponseList.length) {
                                    //iteration over user response list
                                    for (let k = 0; k < userResponseList.length; k++) {
                                        let userResponseObj = userResponseList[k];
                                        if (userResponseObj.selectedOptionValue === 1) {
                                            if (userResponseObj.userId === userId) {
                                                positiveResForUserScore += (1 * userResponseObj.userDefinedWeightage);
                                                positiveResForNationScore += (1 * userResponseObj.userDefinedWeightage);
                                            } else {
                                                positiveResForNationScore += (1 * userResponseObj.userDefinedWeightage);
                                            }

                                        }
                                        if (userResponseObj.selectedOptionValue === -1) {
                                            if (userResponseObj.userId === userId) {
                                                negativeResForUserScore += (1 * userResponseObj.userDefinedWeightage);
                                                negativeResForNationScore += (1 * userResponseObj.userDefinedWeightage);
                                            } else {
                                                negativeResForNationScore += (1 * userResponseObj.userDefinedWeightage);
                                            }

                                        }
                                        if (userResponseObj.selectedOptionValue === 0) {
                                            if (userResponseObj.userId === userId) {
                                                neutralResForUserScore += (1 * userResponseObj.userDefinedWeightage);
                                                neutralResForNationScore += (1 * userResponseObj.userDefinedWeightage);
                                            } else {
                                                neutralResForNationScore += (1 * userResponseObj.userDefinedWeightage);
                                            }

                                        }
                                    } //closing user response list iteration
                                }
                            }
                        }
                        if (positiveResForUserScore || negativeResForUserScore || neutralResForUserScore) {
                            promoterTotal = positiveResForUserScore - negativeResForUserScore - neutralResForUserScore;
                            absoluteTotal = positiveResForUserScore + negativeResForUserScore + neutralResForUserScore;
                            userScore = promoterTotal / absoluteTotal;
                            promoterTotal = positiveResForNationScore - negativeResForNationScore - neutralResForNationScore;
                            absoluteTotal = positiveResForNationScore + negativeResForNationScore + neutralResForNationScore;
                            nationScore = promoterTotal / absoluteTotal;
                            scoreObj.user_score = userScore * 100;
                            scoreObj.nation_score = userScore * 100;
                        } else {
                            scoreObj.user_score = 0;
                            scoreObj.nation_score = 0;
                        }
                        favPartyResponseObj.score.push(scoreObj);
                    }); //closing foreach on data
                    party.find({
                        _id: {
                            $in: partyIds
                        }
                    }).then(parties => {

                        if (parties && parties !== null) {
                            //iteration over parties
                            parties.forEach(party => {

                                favPartyResponseObj.score.map(i => {
                                    if (i.id.toString() === party._id.toString()) {
                                        i.name = party.name;
                                        i.party_logo = party.party_logo;
                                        i.party_name = party.full_name;
                                    }
                                });


                            });
                        }
                        restResponse.sendJsonSuccessResponse(res, favPartyResponseObj);

                    }).catch(err => {
                        restResponse.sendErrorResponse(res, 500, err);
                    });

                } else {
                    restResponse.sendJsonSuccessResponse(res, 'No data found');
                }
            }
        });
    }
    this.getFavouriteGovernment = function(req, res) {
        let responseObj = {};
        let category_satisfaction = [];
        let userId = req.params.userId;

        findGovernmentByName(res, 'Central', function(government) {
            if (government && government !== null) {
                let central = {};
                let state = {};
                findPollsByGovernmentId(res, government._id, function(pollsList) {
                    let score = calculateNationAndUserScoreForGovernment(userId, pollsList);
                    central.name = government.name;
                    central.user_score = score.userScore;
                    central.nation_score = score.nationScore;
                    central.msg = "";
                    central.category_satisfaction = category_satisfaction;
                    responseObj.central = central;

                    findUserById(res, userId, function(user) {
                        findGovernmentByName(res, user.state, function(stateGov) {
                            findPollsByGovernmentId(res, stateGov._id, function(polls) {
                                let stateGovScore = calculateNationAndUserScoreForGovernment(userId, polls);
                                state.name = stateGov.name;
                                state.user_score = stateGovScore.userScore;
                                state.nation_score = stateGovScore.nationScore;
                                state.msg = "";
                                state.category_satisfaction = category_satisfaction;
                                responseObj.state = state;
                                restResponse.sendJsonSuccessResponse(res, responseObj);
                            });

                        });

                    });


                });
                // restResponse.sendJsonSuccessResponse(res, government);
            } else {
                logger.error('error while finding government: ', err);
                restResponse.sendSuccessResponse(res, 404, 'Government not found');
            }
        })
    }

    // Get government by state name
    this.getGovernmentByStateName = function(req, res) {
        /*pending: msg and category_satisfaction*/
        let stateName = req.params.stateName;
        let userId = req.params.userId;
        let responseObj = {
            category_satisfaction: []
        };
        let scoreObj = {};
        let positiveResForUserScore = 0,
            negativeResForUserScore = 0,
            neutralResForUserScore = 0,
            positiveResForNationScore = 0,
            negativeResForNationScore = 0,
            neutralResForNationScore = 0;

        let userScore = 0,
            nationScore = 0,
            promoterTotal = 0,
            absoluteTotal = 0;
        if (stateName) {
            responseObj.name = stateName;
            // responseObj.category_satisfaction = [];
            let category_satisfaction = []
            findGovernmentByName(res, stateName, function(government) {
                if (government && government !== null) {
                    let governmentId = government._id;
                    findPollsByGovernmentId(res, governmentId, function(polls) {
                        let score = calculateNationAndUserScoreForGovernment(userId, polls)
                        responseObj.user_score = score.userScore;
                        responseObj.nation_score = score.nationScore;
                        responseObj.msg = "";
                        //category satisfaction score calculation
                        poll.aggregate([{
                            $group: {
                                _id: "$category_id",
                                "polls": {
                                    $push: "$$ROOT"
                                }
                            }
                        }], function(err, data) {
                            if (err) {
                                logger.error('Error while aggregation on poll', err);
                                restResponse.sendErrorResponse(res, 500, err);
                            } else {
                                if (data && data !== null && data.length) {
                                    let categoryIds = [];
                                    //iteration over list
                                    data.forEach(i => {
                                        categoryIds.push(i._id);
                                        let pollsList = i.polls;
                                        let scoreObj = {
                                            id: i._id
                                        };
                                        // iteration over poll list
                                        if (pollsList && pollsList.length) {


                                            for (let j = 0; j < pollsList.length; j++) {

                                                let poll = pollsList[j];
                                                if (poll.goverment_id === governmentId) {


                                                    let userResponseList = poll.userResponse;
                                                    if (userResponseList && userResponseList.length) {
                                                        //iteration over user response list
                                                        for (let k = 0; k < userResponseList.length; k++) {
                                                            let userResponseObj = userResponseList[k];
                                                            if (userResponseObj.selectedOptionValue === 1) {
                                                                if (userResponseObj.userId === userId) {
                                                                    positiveResForUserScore += (1 * userResponseObj.userDefinedWeightage);
                                                                    positiveResForNationScore += (1 * userResponseObj.userDefinedWeightage);
                                                                } else {
                                                                    positiveResForNationScore += (1 * userResponseObj.userDefinedWeightage);
                                                                }

                                                            }
                                                            if (userResponseObj.selectedOptionValue === -1) {
                                                                if (userResponseObj.userId === userId) {
                                                                    negativeResForUserScore += (1 * userResponseObj.userDefinedWeightage);
                                                                    negativeResForNationScore += (1 * userResponseObj.userDefinedWeightage);
                                                                } else {
                                                                    negativeResForNationScore += (1 * userResponseObj.userDefinedWeightage);
                                                                }

                                                            }
                                                            if (userResponseObj.selectedOptionValue === 0) {
                                                                if (userResponseObj.userId === userId) {
                                                                    neutralResForUserScore += (1 * userResponseObj.userDefinedWeightage);
                                                                    neutralResForNationScore += (1 * userResponseObj.userDefinedWeightage);
                                                                } else {
                                                                    neutralResForNationScore += (1 * userResponseObj.userDefinedWeightage);
                                                                }

                                                            }
                                                        } //closing user response list iteration
                                                    }
                                                }
                                            } // closing iteration over poll list
                                        }
                                        if (positiveResForUserScore || negativeResForUserScore || neutralResForUserScore) {
                                            promoterTotal = positiveResForUserScore - negativeResForUserScore - neutralResForUserScore;
                                            absoluteTotal = positiveResForUserScore + negativeResForUserScore + neutralResForUserScore;
                                            userScore = promoterTotal / absoluteTotal;
                                            promoterTotal = positiveResForNationScore - negativeResForNationScore - neutralResForNationScore;
                                            absoluteTotal = positiveResForNationScore + negativeResForNationScore + neutralResForNationScore;
                                            nationScore = promoterTotal / absoluteTotal;
                                            scoreObj.user_score = userScore * 100;
                                            scoreObj.nation_score = userScore * 100;
                                        } else {
                                            scoreObj.user_score = 0;
                                            scoreObj.nation_score = 0;
                                        }
                                        responseObj.category_satisfaction.push(scoreObj);
                                    });
                                    // restResponse.sendJsonSuccessResponse(res, responseObj);
                                    Category.find({
                                        _id: {
                                            $in: categoryIds
                                        }

                                    }).then(categories => {
                                        if (categories && categories.length) {
                                            categories.forEach(category => {
                                                responseObj.category_satisfaction.map(i => {
                                                    if (category._id.toString() === i.id.toString()) {
                                                        i.govt_name = stateName;
                                                        i.category_id = category._id;
                                                        i.category_name = category.name;
                                                        i.category_image = category.category_image;
                                                    }
                                                });
                                            });
                                            restResponse.sendJsonSuccessResponse(res, responseObj);

                                        }
                                    }).catch(err => {
                                        logger.error('Error in finding members: ', err);
                                        restResponse.sendErrorResponse(res, 500, err);
                                    });

                                }
                            }
                        }); //closing poll aggregation

                    });
                }
            });

        }



    };
    this.getPromiseTrack = function(req, res) {
        let userId = req.params.userId;
        let responseObj = {};
        let promiseStatus = {};
        let positiveResForUserScore = 0,
            negativeResForUserScore = 0,
            neutralResForUserScore = 0,
            positiveResForNationScore = 0,
            negativeResForNationScore = 0,
            neutralResForNationScore = 0;
        findGovernmentByName(res, 'Central', function(government) {
            if (government && government !== null) {
                findPollsByGovernmentIdAndPollCategoryPromise(res, government._id, function(polls) {
                    console.log('polls: ', polls);
                    let promiseTracStatus = calculatePromiseTrackStatus(polls);

                    promiseStatus.Central = promiseTracStatus;
                    responseObj.promiseStatus = promiseStatus;
                    findUserById(res, userId, function(user) {
                        findGovernmentByName(res, user.state, function(government) {
                            findPollsByGovernmentId(res, government._id, function(polls) {
                                promiseTracStatus = calculatePromiseTrackStatus(polls);
                                let stateName = user.state;
                                promiseStatus[stateName] = promiseTracStatus;
                                responseObj.promiseStatus = promiseStatus;
                                restResponse.sendJsonSuccessResponse(res, responseObj);
                            });

                        });

                    }); //closing user finding

                });
            }
        });


    }

    function findGovernmentByName(res, name, callback) {
        Government.findOne({
            name: name
        }, function(err, government) {
            if (err) {
                restResponse.sendErrorResponse(res, 500, err);
            } else {
                if (government !== null) {
                    callback(government);
                } else {
                    logger.error('Error while finding government by name: ');
                    restResponse.sendSuccessResponse(res, 404, "Government not found");
                }

            }
        });
    }

    function findPollsByGovernmentId(res, governmentId, callback) {
        poll.find({
            government_id: governmentId
        }, function(err, polls) {

            if (err) {
                restResponse.sendErrorResponse(res, 500, err);
            } else {
                if (polls.length) {
                    callback(polls);
                } else {
                    restResponse.sendSuccessResponse(res, 404, 'Poll not found');
                }


            }
        });
    }

    function findPollsByGovernmentIdAndPollCategoryPromise(res, governmentId, callback) {

        poll.find({
            $and: [{
                government_id: governmentId
            }, {
                pollCategory: 'Promise'
            }]
        }, function(err, polls) {
            if (err) {
                logger.error('Error in finding polls: ', err);
                restResponse.sendErrorResponse(res, 500, err);
            } else {
                if (polls.length) {
                    callback(polls);
                } else {
                    restResponse.sendSuccessResponse(res, 404, 'Poll not found');
                }



            }
        });
    }

    function findUserById(res, userId, callback) {
        User.findById({
            _id: userId
        }, function(err, user) {
            if (err) {
                restResponse.sendErrorResponse(res, 500, err);
            } else {
                if (user !== null) {
                    callback(user);
                } else {
                    restResponse.sendSuccessResponse(res, 404, "User not found");
                }

            }
        });
    }
    /*This fuction will calculate user score and nation score by using poll response*/
    function calculateNationAndUserScoreForGovernment(userId, pollList) {
        let scoreObj = {};
        let positiveResForUserScore = 0,
            negativeResForUserScore = 0,
            neutralResForUserScore = 0,
            positiveResForNationScore = 0,
            negativeResForNationScore = 0,
            neutralResForNationScore = 0;
        let totalNumberOfResponse = 0,
            totalNumberOfUserResponse = 0;
        let userScore = 0,
            nationScore = 0;
        // iteration over poll list
        for (let j = 0; j < pollList.length; j++) {
            let poll = pollList[j];
            let userResponseList = poll.userResponse;
            if (userResponseList && userResponseList.length) {
                //iteration over user response list
                for (let k = 0; k < userResponseList.length; k++) {
                    let userResponseObj = userResponseList[k];
                    if (userResponseObj.selectedOptionValue === 1) {
                        if (userResponseObj.userId === userId) {
                            positiveResForUserScore += 1;
                            positiveResForNationScore += 1;
                        } else {
                            positiveResForNationScore += 1;
                        }

                    }
                    if (userResponseObj.selectedOptionValue === -1) {
                        if (userResponseObj.userId === userId) {
                            negativeResForUserScore += 1;
                            negativeResForNationScore += 1;
                        } else {
                            negativeResForNationScore += 1;
                        }

                    }
                    if (userResponseObj.selectedOptionValue === 0) {
                        if (userResponseObj.userId === userId) {
                            neutralResForUserScore += 1;
                            neutralResForNationScore += 1;
                        } else {
                            neutralResForNationScore += 1;
                        }

                    }
                } //closing user response list iteration
            }
        }
        totalNumberOfResponse = positiveResForNationScore + negativeResForNationScore + neutralResForNationScore;
        totalNumberOfUserResponse = positiveResForUserScore + negativeResForUserScore + neutralResForUserScore;
        if (totalNumberOfUserResponse) {
            userScore = (positiveResForUserScore / totalNumberOfUserResponse) * 100;
        } else {
            userScore = 0;
        }
        if (totalNumberOfResponse) {
            nationScore = (positiveResForNationScore / totalNumberOfResponse) * 100;
        } else {
            nationScore = 0;
        }
        scoreObj.userScore = userScore;
        scoreObj.nationScore = nationScore;
        return scoreObj;
    }

    function calculatePromiseTrackStatus(pollList) {
        let positiveResCount = 0,
            negativeResCount = 0,
            neutralResCount = 0;
        // iteration over poll list
        for (let j = 0; j < pollList.length; j++) {
            let poll = pollList[j];
            let userResponseList = poll.userResponse;
            if (userResponseList && userResponseList.length) {
                //iteration over user response list
                for (let k = 0; k < userResponseList.length; k++) {
                    let userResponseObj = userResponseList[k];
                    if (userResponseObj.selectedOptionValue === 1) {
                        positiveResCount += 1;
                    }
                    if (userResponseObj.selectedOptionValue === -1) {
                        negativeResCount += 1;
                    }
                    if (userResponseObj.selectedOptionValue === 0) {
                        neutralResCount += 1;

                    }
                } //closing user response list iteration
            }
        }
        if (positiveResCount >= (negativeResCount + neutralResCount)) {
            return 'UP';
        } else {
            return 'DOWN';
        }
    }

}
module.exports.DashboardController = DashboardController;