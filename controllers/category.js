const mongoose = require('mongoose');
const Category = mongoose.model('category');

let logger = require('../utils/logger.js');
let restResponse = require('../utils/restresponse.js');


function CategoryController() {
    this.getCategoryById = function(req, res, next) {
        let categoryId = req.params.categoryid;
        console.log('Id to find category is: ', categoryId);
        Category.findOne({
            _id: categoryId
        }).exec(function(err, category) {
            if (err) {
                logger.error('Error while finding category: ', err);
                restResponse.sendErrorResponse(res, 500, err);
            } else {
                if (category !== null) {
                    restResponse.sendJsonSuccessResponse(res, category);
                } else {
                    restResponse.sendSuccessResponse(res, 404, "Category not found!");
                }

            }

        });
    }

    this.createCategory = function(req, res, next) {
        let reqBody = req.body;
        if (reqBody) {
            let category = new Category();
            category.name = reqBody.name;
            category.category_image = reqBody.category_image;
            category.status = reqBody.status;
            category.is_mobile = reqBody.is_mobile;
            category.save(function(err, category) {
                if (err) {
                    logger.error('Error in category creation: ', err);
                    restResponse.sendErrorResponse(res, 500, err);
                } else {
                    restResponse.sendJsonSuccessResponse(res, category);
                }
            })
        } //closing if
    }
}

module.exports.CategoryController = CategoryController;