const mongoose = require( 'mongoose' );
const Schema = mongoose.Schema;
const GovernmentSchema = new Schema( {
  name: {
    type: String,
    trim: true,
    index:true
  },
  email: {
    type: String,
    trim: true
  },
  ruling_party: {
    type: String,
    trim: true
  },
  opposition_party: {
    type: String,
    trim: true
  },
  moreinfo: {
    type: String,
    trim: true
  },
  logo: {
    type: String,
    trim: true
  },
  background_image: {
    type: String,
    trim: true
  },
  colation_goverment: {
    type: String,
    trim: true
  },
  start_date: {
    type: Date
  },
  end_date: {
    type: Date
  },
  status: {
    type: String,
    enum: [ 'Active', 'Inactive' ]
  },
  created_on: {
    type: Date,
    default: new Date()
  }
} );

mongoose.model('Government',GovernmentSchema);
