const mongoose = require( 'mongoose' );
const Party = mongoose.model( 'party' );
const restResponse = require( '../utils/restresponse.js' );

function PartyController() {
  this.createParty = function( req, res ) {
    let createPartyReq = req.body;
    let party = new Party();
    party.name = createPartyReq.name;
    party.full_name = createPartyReq.full_name;
    party.email = createPartyReq.email;
    party.party_logo = createPartyReq.party_logo;
    party.status = createPartyReq.status;
    party.background_image = createPartyReq.background_image;
    party.save( party, function( err, party ) {
      if ( err ) {
        restResponse.sendErrorResponse( res, 500, err )
      } else {
        restResponse.sendJsonSuccessResponse( res, party );
      }
    } );
  }
}
module.exports.PartyController = PartyController;
