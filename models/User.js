const mongoose = require( 'mongoose' );
const Schema = mongoose.Schema;
const UserSchema = new Schema( {
  "device_id": {
    type: String,
    trim: true,
    required: true,
    unique: true,
    index: true
  },
  "email": {
    type: String,
    trim: true,
    required: true
  },
  "gcm_token": {
    type: String,
    trim: true
  },
  "image_url": {
    type: String,
    trim: true
  },
  "name": {
    type: String,
    trim: true
  },
  "state": {
    type: String,
    trim: true
  },
  "response_count": {
    type: Number,
    trim: true
  },
  "selected_categories_id": {
    type: Array
  },
  "message": {
    type: String,
    trim: true
  }
} );
mongoose.model( 'user', UserSchema )
