const config = require('../config/config.js')
let favCandidateResponseObj = {
    score: [],
    msg: {
        liked: config.likedTextForCandidate,
        disliked: config.dislikedTextForCandidate
    }

};
module.exports = favCandidateResponseObj;