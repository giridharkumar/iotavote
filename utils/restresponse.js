let restResponse = {
	sendErrorResponse: function(res,code,err){
		res.status(code);
		res.json({
			success:false,
			data:{
				message:'Error occurred: '+err
			}
		});
	},
	sendSuccessResponse:function(res, code, message){
		res.status(code);
		res.json({
			success:true,
			data:{
				message:message
			}
		});
	},
	sendJsonSuccessResponse: function(res, data){
		res.status(200);
		res.json({
			success:true,
			 data:data
		});
	}
};

	module.exports = restResponse;