const fs = require( 'fs' );
module.exports = function( app ) {
  let controllers = setupControllers();
  // console.log('contrllers: ',controllers);
  let userController = new controllers.user.UserController();
  let pollController = new controllers.poll.PollController();
  let dashboardCotroller = new controllers.dashboard.DashboardController();
  let partyController = new controllers.party.PartyController();
  let governmentController = new controllers.government.GovernmentController();
  let partyMemberController = new controllers.partymember.PartyMemberController();
  let regionController = new controllers.region.RegionController();
  let categoryController = new controllers.category.CategoryController();
  //user apis
  
  /*this api will be used to register user*/
  app.post( "/api/user/register", userController.register );
  /*this api will be used to get user details*/
  app.get( "/api/user/:userid", userController.getUser );
  /*this api will be used to get update user details*/
  app.put( '/api/user/update-profile', userController.updateUserProfile );
  /*this api will be used to update user's state and category*/
  app.put( '/api/user/update-state-and-category', userController.updateStateAndCategory );
  /*this api will be used to get state list based on the country code*/
  // app.get( 'api/user/state', userController.getStateList );

  //category apis
  /*this api will be used to get category list*/
  app.get( '/api/poll/category-list', userController.getCategoryList );
  // for CRUD operation
  app.get('/api/poll/category/:categoryid',categoryController.getCategoryById);
  app.post('/api/poll/category',categoryController.createCategory);

  /*Poll APIs*/

  /*This will return poll or list of poll on the basis of request params*/
  app.get('/api/poll',pollController.getPoll);
  app.put('/api/poll',pollController.updatePoll);
  app.post('/api/poll',pollController.createPoll);

  /*Create party*/
  app.post('/api/party',partyController.createParty);

  /*Create government*/
  app.post('/api/government',governmentController.createGovernment);

  /*Create party member*/
  app.post('/api/party-member',partyMemberController.createPartyMember);
  /*Create region*/
  app.post('/api/region',regionController.createRegion);

  /*Dashboard APIs*/
  // 10) Get Favourite Party:
  app.get('/api/dashboard/favourite-party/:userId',dashboardCotroller.getFavouriteParty);
  // 11) Get Favourite Candidate:
  app.get('/api/dashboard/favourite-candidate/:userId',dashboardCotroller.getFavouriteCandidate);
  // 12) Get Favourite Government :
  app.get('/api/dashboard/favourite-government/:userId',dashboardCotroller.getFavouriteGovernment);
  //13) Get Favourite Government by state name:
  app.get('/api/dashboard/government-by-statename/:userId/:stateName',dashboardCotroller.getGovernmentByStateName);
  //14) Get Government Detail:
  // app.get('/api/dashboard/government-detail/:userId/:stateName',)
  //15) Get Promise Track:
  app.get('/api/dashboard/promise-track/:userId',dashboardCotroller.getPromiseTrack);

};

function setupControllers() {
  let controllers = {};
  let controllersPath = process.cwd() + '/controllers';
  fs.readdirSync( controllersPath ).forEach( function( file ) {
    if ( file.indexOf( '.js' ) != -1 ) {
      controllers[ file.split( '.' )[ 0 ] ] = require( controllersPath + '/' + file );
    }
  } );
  return controllers;
}
