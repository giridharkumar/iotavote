const mongoose = require('mongoose');
const PartyMember = mongoose.model('partymember');
const restResponse = require('../utils/restresponse.js');

function PartyMemberController() {
    this.createPartyMember = function(req, res) {
        let createPartyMemberReq = req.body;
        let partyMember = new PartyMember();
        partyMember.name = createPartyMemberReq.name;
        partyMember.email = createPartyMemberReq.email;
        partyMember.member_pic = createPartyMemberReq.member_pic;
        partyMember.status = createPartyMemberReq.status;
        partyMember.sub_role = createPartyMemberReq.sub_role;
        partyMember.is_goverment = createPartyMemberReq.is_goverment;
        partyMember.party_id = createPartyMemberReq.party_id;
        partyMember.party_role_id = createPartyMemberReq.party_role_id;
        partyMember.public_role_id = createPartyMemberReq.public_role_id;
        partyMember.region_id = createPartyMemberReq.region_id;
        partyMember.goverment_id = createPartyMemberReq.goverment_id;
        partyMember.background_image = createPartyMemberReq.background_image;
        partyMember.save(partyMember, function(err, partyMember) {
            if (err) {
                restResponse.sendErrorResponse(res, 500, err);
            } else {
                restResponse.sendJsonSuccessResponse(res, partyMember);
            }
        });


    }
}
module.exports.PartyMemberController = PartyMemberController;