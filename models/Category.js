const mongoose = require( 'mongoose' );
const Schema = mongoose.Schema;
const CategorySchema = new Schema( {
  "name": {
    type: String,
    trim: true
  },
  "category_image": {
    type: String,
    trim: true
  },
  "status": {
    type: String,
    enum: [ "Active", "Inactive" ]
  },
  "is_mobile": {
    type: String,
    trim: true
  },
  "deleted": {
    type: String,
    trim: true
  },
  "deleted_by": {
    type: String,
    trim: true
  },
  "created_on": {
    type: Date,
    default: Date.now
  },
  "created_by": {
    type: String,
    trim: true
  },
  "modified_on": {
    type: Date
  },
  "modified_by": {
    type: String,
    trim: true
  }
} );
mongoose.model( 'category', CategorySchema );
