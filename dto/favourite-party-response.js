const config = require('../config/config.js');
let favPartyResponseObj = {
    score: [],
    msg: {
        liked: config.likedTextForParty,
        disliked: config.dislikedTextForParty
    }

};
module.exports = favPartyResponseObj;