const mongoose = require( 'mongoose' );
const Government = mongoose.model( 'Government' );
const restResponse = require('../utils/restresponse.js');
const logger = require('../utils/logger.js');

function GovernmentController() {
  this.createGovernment = function( req, res ) {
    let governmentCreationReq = req.body;
    let government = new Government();
    government.name = governmentCreationReq.name;
    government.email = governmentCreationReq.name;
    government.ruling_party = governmentCreationReq.ruling_party;
    government.opposition_party = governmentCreationReq.opposition_party;
    government.moreinfo = governmentCreationReq.moreinfo;
    government.logo = governmentCreationReq.logo;
    government.background_image = governmentCreationReq.background_image;
    government.colation_goverment = governmentCreationReq.colation_goverment;
    government.start_date = governmentCreationReq.start_date;
    government.end_date = governmentCreationReq.end_date;
    government.status = governmentCreationReq.status;
    government.save(government,function(err,government){
    	if(err){
    		logger.error('Error while creating government',err);
    		restResponse.sendErrorResponse(res,500,err);
    	}else{
    		restResponse.sendJsonSuccessResponse(res,government);
    	}

    });
  }
}
module.exports.GovernmentController = GovernmentController;
