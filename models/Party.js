const mongoose = require( 'mongoose' );
const Schema = mongoose.Schema;
const partySchema = new Schema( {
  "name": {
    type: String,
    trim: true,
    required:true,
  },
  full_name: {
    type: String,
    trim: true,
    required:true
  },
  email: {
    type: String,
    trim: true
  },
  party_logo: {
    type: String,
    trim: true
  },
  status: {
    type: String,
    enum: [ 'Active', 'Inactive' ]
  },
  created_on: {
    type: Date,
    default: new Date()
  },
  modified_on: {
    type: Date
  },
  background_image: {
    type: String,
    trim: true
  }
} );
mongoose.model( 'party', partySchema );
