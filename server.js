'use strict';
const restify = require( 'restify' );
const bodyParser = require( 'body-parser' );
/*query parser for parsing query from url*/
const queryParser = require( 'query-parser' );
const mongoose = require( 'mongoose' );
/*For implementing pagination*/
let paginate = require( 'restify-paginate' );
/* DeprecationWarning: Mongoose: mpromise (mongoose's default promise library) is deprecated, plug in your own promise library
 */
mongoose.Promise = global.Promise;
const config = require( './config/config.js' );
const glob = require( 'glob' );
//sync models
var models = glob.sync( './models/*.js' );
models.forEach( function( model ) {
  require( model );
} );
// create server
let server = restify.createServer();
server.use( restify.plugins.bodyParser() );
server.use( bodyParser.json());
// server.use( restify.fullResponse() );
server.use( paginate( server ) );
/*using query parser for parsing query from url*/
 server.use( restify.plugins.queryParser() );
require( './apis/apis' )( server );
//MongoDB connection
mongoose.connect( config.development.db );
let mongoDBConnection = mongoose.connection;
mongoDBConnection.on( 'error', console.error.bind( console, 'mongo connection error:' ) );
mongoDBConnection.once( 'open', function() {
  console.log( 'MongoDB is connected' );
} );
let port = process.env.PORT || config.development.port;
server.listen( port, function( err ) {
  if ( err ) {
    console.log( 'error on listening port: ', err );
  } else {
    console.log( 'Server running on : ' + port );
  }
} );
