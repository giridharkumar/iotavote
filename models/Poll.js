const mongoose = require('mongoose');
const Schema = mongoose.Schema;
const pollSchema = new Schema({
    'poll_to_user': {
        type: String,
        trim: true
    },
    'question_to_user': {
        type: String,
        trim: true
    },
    'positive_option_text': {
        type: String,
        trim: true
    },
    'negative_option_text': {
        type: String,
        trim: true
    },
    'neutral_option_text': {
        type: String,
        trim: true
    },
    'source_of_information_text': {
        type: String,
        trim: true
    },
    'picture_background': {
        type: String,
        trim: true
    },
    'poll_weightage': {
        type: Number
    },
    'poll_type': {
        type: String,
        trim: true
    },
    'status': {
        type: String,
        enum: ['Active', 'Inactive']
    },
    'status_of_promise': {
        type: String,
        enum: ['Not Started', 'On Track', 'Delayed', 'Complete']
    },
    'deleted': {
        type: Number
    },
    'deleted_by': {
        type: String,
        trim: true
    },
    'created_on': {
        type: Date
    },
    'publish_in_media_on': {
        type: Date
    },
    'poll_sent_on': {
        type: Date
    },
    'created_by': {
        type: String,
        trim: true
    },
    'modified_on': {
        type: Date
    },
    'modified_by': {
        type: String,
        trim: true
    },
    'member_id': {
        type: String,
        trim: true,
        index: true
    },
    'party_id': {
        type: String,
        trim: true,
        index: true
    },
    'region_id': {
        type: String,
        trim: true,
        index: true
    },
    'topic_id': {
        type: String,
        trim: true,
        index: true
    },
    'government_id': {
        type: String,
        trim: true,
        index: true
    },
     'category_id': {
        type: String,
        trim: true,
        index: true
    },
    'userResponse': [{
        '_id': false,
        'userId': {
            type: String,
            trim: true,
            unique: true
        },
        'selectedOptionValue': {
            'type': Number
        },
        userDefinedWeightage: {
            type: Number
        }
    }],
    pollCategory: {
        type: String,
        enum: ['Poll', 'Promise', 'NA']
    },
    notificationStates: [{
        stateName: {
            type: String,
            trim: true
        }
    }],
    // collection logical group of polls
    collectionOfPolls: [{
        pollId: {
            type: String,
            trim: true
        }
    }]
});
mongoose.model('poll', pollSchema);