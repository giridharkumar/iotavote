const mongoose = require( 'mongoose' );
let User = mongoose.model( "user" );
let Category = mongoose.model( 'category' );
let logger = require( '../utils/logger.js' );
let restResponse = require( '../utils/restresponse.js' );

function UserController() {
  //register user
  this.register = function( req, res, next ) {
      if ( req.body ) {
        let userCreationRequest = req.body;
        //check if user exists by device id
        User.findOne( {
          device_id: userCreationRequest.device_id
        } ).exec( function( err, userFromDb ) {
          if ( userFromDb ) {
            userFromDb.message = 'The device has already been registered!'
            restResponse.sendJsonSuccessResponse( res, userFromDb );
          } else {
            let user = new User();
            user.device_id = userCreationRequest.device_id;
            user.email = userCreationRequest.email;
            user.gcm_token = userCreationRequest.gcm_token;
            user.image_url = userCreationRequest.image_url;
            user.name = userCreationRequest.name;
            user.state = userCreationRequest.state;
            user.response_count = userCreationRequest.response_count;
            user.save( function( err, user ) {
              if ( err ) {
                restResponse.sendErrorResponse( res, 500, err );
              } else {
                restResponse.sendJsonSuccessResponse( res, user );
              }
            } );
          }
        } );
      }
    }
    /* GET user . */
  this.getUser = function( req, res, next ) {
    let userId = req.params.userid;
    if ( userId ) {
      User.findOne( {
        _id: userId
      } ).exec( function( err, user ) {
        if ( err ) {
          restResponse.sendErrorResponse( res, 500, err );
        } else {
          //logger.info('user: ',user);
          if ( user !== null ) {
            restResponse.sendJsonSuccessResponse( res, user );
          }
        }
      } );
    }
  };
  /*Update user profile*/
  this.updateUserProfile = function( req, res, next ) {
    let updateRequest = req.body;
    User.findOne( {
      _id: updateRequest.user_id
    } ).exec( function( err, user ) {
      if ( err ) {
        restResponse.sendErrorResponse( res, 500, err );
      } else {
        if ( user !== null ) {
          if ( updateRequest.name ) {
            user.name = updateRequest.name;
          }
          if ( updateRequest.email ) {
            user.email = updateRequest.email;
          }
          if ( updateRequest.state ) {
            user.state = updateRequest.state;
          }
          if ( updateRequest.gcm_token ) {
            user.gcm_token = updateRequest.gcm_token;
          }
          user.save( function( err, user ) {
            if(err){
              restResponse.sendErrorResponse(res,500,err);
            }else{
              restResponse.sendJsonSuccessResponse(res, user);
            }
          } );
        }
      }
    } );
  };
  /*update state and country to user*/
  this.updateStateAndCategory = function( req, res, next ) {
      let updateRequest = req.body;
      logger.info( "User update request: ", updateRequest );
      if ( updateRequest ) {
        User.findOne( {
          _id: updateRequest.user_id
        } ).exec( function( err, user ) {
          if ( err ) {
            logger.info( 'Error in getting user: ', err );
            restResponse.sendErrorResponse( res, 500, err );
          } else {
            // logger.info('User to be updated: ',user);
            user.state = updateRequest.state;
            let categoryIdList = user.selected_categories_id.concat( updateRequest.category_id );
            user.selected_categories_id = Array.from( new Set( categoryIdList ) );
            user.country = updateRequest.country;
            user.save( function( err, updatedUser ) {
              if ( err ) {
                logger.error( 'Error in saving user', err );
                restResponse.sendErrorResponse( res, 500, err );
              } else {
                restResponse.sendSuccessResponse( res, 200, 'Updated Successfully!' );
              }
            } );
          }
        } );
      }
    }
    /*GET category list*/
  this.getCategoryList = function( req, res, next ) {
      Category.find( {} ).exec( function( err, categories ) {
        if ( err ) {
          logger.error('Error while getting category list: ',err);
          restResponse.sendErrorResponse(res,500,err);
        } else {
          restResponse.sendJsonSuccessResponse(res,categories);
          
        }
      } );
    }
    
} //closing UserController
module.exports.UserController = UserController;
