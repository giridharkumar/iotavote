const mongoose = require('mongoose');
let Poll = mongoose.model('poll');
let ObjectId = mongoose.Types.ObjectId;
let restResponse = require('../utils/restresponse.js')

function PollController() {
    /*This function will return poll or list of poll on the basis of request params*/
    this.getPoll = function(req, res, next) {
        let queryParams = req.query;
        let page = queryParams.page;
        let perPage = queryParams.perPage;
        if (queryParams.userId && !queryParams.pollId) {
            Poll.find({}).skip(parseInt(page)).limit(parseInt(perPage)).exec(function(err, polls) {
                if (err) {
                    restResponse.sendErrorResponse(res, 500, err);
                } else {
                    let updatedPollList = [];
                    for (let i = 0; i < polls.length; i++) {
                        let updatedPollResponse = polls[i].toObject();
                        let totalNumberOfPositiveVote = 0,
                            totalNumberOfNegativeVote = 0,
                            totalNumberOfNeutralvote = 0,
                            totalNumberOfVote;
                        /*check if user is already voted on this poll*/
                        let userResponse = polls[i].userResponse;
                        if (userResponse.length > 0) {
                            let isUserVoted = false;
                            updatedPollResponse.isVoted = 0;
                            if (userResponse.indexOf(queryParams.userId) !== -1) {
                                isUserVoted = true;
                                updatedPollResponse.isVoted = 1;
                            }
                            totalNumberOfVote = updatedPollResponse.userResponse.length;
                            /*iteration for finding total number of 1,-1,0 voted vote*/
                            for (let i = 0; i < totalNumberOfVote; i++) {
                                if (updatedPollResponse.userResponse[i].selectedOptionValue === 1) {
                                    totalNumberOfPositiveVote++;
                                }
                                if (updatedPollResponse.userResponse[i].selectedOptionValue === -1) {
                                    totalNumberOfNegativeVote++;
                                }
                                if (updatedPollResponse.userResponse[i].selectedOptionValue === 0) {
                                    totalNumberOfNeutralvote++;
                                }
                            }
                            updatedPollResponse.nationWiseGood = (totalNumberOfPositiveVote / totalNumberOfVote) * 100;
                            updatedPollResponse.nationWiseBad = (totalNumberOfNegativeVote / totalNumberOfVote) * 100;
                            updatedPollResponse.nationWiseNormal = (totalNumberOfNeutralvote / totalNumberOfVote) * 100;
                        }
                        updatedPollList.push(updatedPollResponse)
                    }
                    restResponse.sendJsonSuccessResponse(res, updatedPollList);
                }
            });
        }
        if (queryParams.pollId) {
            Poll.findById({
                _id: queryParams.pollId
            }).exec(function(err, poll) {
                if (err) {
                    restResponse.sendErrorResponse(res, 500, err);
                } else {
                    let updatedPollResponse = poll.toObject();
                    let totalNumberOfPositiveVote = 0,
                        totalNumberOfNegativeVote = 0,
                        totalNumberOfNeutralvote = 0,
                        totalNumberOfVote;
                    /*check if user is already voted on this poll*/
                    if (poll.userResponse.length > 0) {
                        let isUserVoted = false;
                        for (let i = 0; i < poll.userResponse.length; i++) {
                            if (poll.userResponse[i].userId === queryParams.userId) {
                                isUserVoted = true;
                            }
                        }
                        if (isUserVoted) {
                            updatedPollResponse.isVoted = 1;
                        } else {
                            updatedPollResponse.isVoted = 0;
                        }
                    }
                    totalNumberOfVote = poll.userResponse.length;
                    /*iteration for finding total number of 1,-1,0 voted vote*/
                    for (let i = 0; i < totalNumberOfVote; i++) {
                        if (poll.userResponse[i].selectedOptionValue === 1) {
                            totalNumberOfPositiveVote++;
                        }
                        if (poll.userResponse[i].selectedOptionValue === -1) {
                            totalNumberOfNegativeVote++;
                        }
                        if (poll.userResponse[i].selectedOptionValue === 0) {
                            totalNumberOfNeutralvote++;
                        }
                    }
                    updatedPollResponse.nationWiseGood = (totalNumberOfPositiveVote / totalNumberOfVote) * 100;
                    updatedPollResponse.nationWiseBad = (totalNumberOfNegativeVote / totalNumberOfVote) * 100;
                    updatedPollResponse.nationWiseNormal = (totalNumberOfNeutralvote / totalNumberOfVote) * 100;
                    restResponse.sendJsonSuccessResponse(res, updatedPollResponse);
                }
            });
        }
    };
    this.updatePoll = function(req, res) {
        let pollUpdateRequest = req.body;
        Poll.findById({
            _id: pollUpdateRequest.pollId
        }).exec(function(err, poll) {
            if (err) {
                restResponse.sendErrorResponse(res, 500, err);
            } else {
                let userResponse = {
                    userId: pollUpdateRequest.userId,
                    slectedOption: pollUpdateRequest.slectedOption,
                    selectedOptionValue: pollUpdateRequest.selectedOptionValue,
                    userDefinedWeightage: pollUpdateRequest.userDefinedWeightage
                };
                /*check if user is already voted on this poll*/
                if (poll.userResponse.length > 0) {
                    let isUserVoted = false;
                    for (let i = 0; i < poll.userResponse.length; i++) {
                        if (poll.userResponse[i].userId === pollUpdateRequest.userId) {
                            isUserVoted = true;
                        }
                    }
                    /*change logic here if user can change his vote*/
                    if (!isUserVoted) {
                        poll.userResponse.push(userResponse);
                    }
                } else {
                    poll.userResponse.push(userResponse);
                }
                poll.save(function(err, updatedPoll) {
                    if (err) {
                        restResponse.sendErrorResponse(res, 500, err);
                    } else {
                        let updatedPollResponse = updatedPoll.toObject();
                        let totalNumberOfPositiveVote = 0,
                            totalNumberOfNegativeVote = 0,
                            totalNumberOfNeutralvote = 0,
                            totalNumberOfVote;
                        updatedPollResponse.isVoted = 1;
                        totalNumberOfVote = updatedPoll.userResponse.length;
                        /*iteration for finding total number of 1,-1,0 voted vote*/
                        for (let i = 0; i < totalNumberOfVote; i++) {
                            if (updatedPoll.userResponse[i].selectedOptionValue === 1) {
                                totalNumberOfPositiveVote++;
                            }
                            if (updatedPoll.userResponse[i].selectedOptionValue === -1) {
                                totalNumberOfNegativeVote++;
                            }
                            if (updatedPoll.userResponse[i].selectedOptionValue === 0) {
                                totalNumberOfNeutralvote++;
                            }
                        }
                        updatedPollResponse.nationWiseGood = (totalNumberOfPositiveVote / totalNumberOfVote) * 100;
                        updatedPollResponse.nationWiseBad = (totalNumberOfNegativeVote / totalNumberOfVote) * 100;
                        updatedPollResponse.nationWiseNormal = (totalNumberOfNeutralvote / totalNumberOfVote) * 100;
                        restResponse.sendJsonSuccessResponse(res, updatedPollResponse);
                    }
                });
            }
        });
    }
    this.createPoll = function(req, res) {
        let pollCreationReq = req.body;
        let poll = new Poll();
        poll.poll_to_user = pollCreationReq.poll_to_user;
        poll.question_to_user = pollCreationReq.question_to_user;
        poll.positive_option_text = pollCreationReq.negative_option_text;
        poll.neutral_option_text = pollCreationReq.neutral_option_text;
        poll.source_of_information_text = pollCreationReq.source_of_information_text;
        poll.picture_background = pollCreationReq.picture_background;
        poll.poll_weightage = pollCreationReq.poll_weightage;
        poll.poll_type = pollCreationReq.poll_type;
        poll.status = pollCreationReq.status;
        poll.status_if_promise = pollCreationReq.status_if_promise;
        poll.publish_in_media_on = pollCreationReq.publish_in_media_on;
        poll.poll_sent_on = pollCreationReq.poll_sent_on;
        poll.member_id = pollCreationReq.member_id;
        poll.party_id = pollCreationReq.party_id;
        poll.region_id = pollCreationReq.region_id;
        poll.topic_id = pollCreationReq.topic_id;
        poll.goverment_id = pollCreationReq.goverment_id;
        poll.pollCategory = pollCreationReq.pollCategory;
        poll.save(poll, function(err, poll) {
            if (err) {
                restResponse.sendErrorResponse(res, 500, err);
            } else {
                restResponse.sendJsonSuccessResponse(res, poll);
            }
        });
    }
} //closing PollController
module.exports.PollController = PollController;